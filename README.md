# CRUD App with React & Firebase

This small app is the 7-day challeging project in my progress of learning ReactJS. The final product allows to create, update and delete items on database. It's built with React.js, Redux, Firebase and other wonderful libraries like react-router, moment.js, axios, etc.

Since the timeframe to complete the project is limited, the end result aims to fulfill the project requirements, assure coding quality and well-organized structure.

The project contains some limitations and they will continue to be improved or added.

1.  Since Firebase does not provide a flexible way to query data like traditional databases, the paging function will need more time to be done correctly. Currently, this project is dealing with the pagination issue by loading all entries from the first call.
2.  There is no Authentication and Login Page.
3.  Aesthetic and responsiveness are not the priority, components and layouts are orginized by Bootstrap grid classes.
4.  Audio and Video are played by the default HTML5 player.
5.  Browsers do not allow to directly download media from external resources.

## Getting Started

The project was initized by `create-react-app`. Following are steps to install and run it locally.

1.  Make sure you have `git`, `node` amd `npm` installed
2.  Clone this repository locally
3.  Execute `npm install` then `npm start`
4.  Open `http://localhost:3000` in your browser
