import config from "./config";
import { isString } from "lodash";

/**
 * Return the extension from the given media link
 * For example, domain.com/mussic.mp3 ---> mp3
 *
 * @param {*} mediaLink Link of image, video or audio file
 */
export function getMediaExtension(mediaLink) {
  const tokens = mediaLink.split(".");
  return tokens[tokens.length - 1].toLowerCase();
}

/**
 * Check the given link to match the valid file extensions
 * For example, .mp3 -> true .json -> false
 *
 * @param {*} mediaLink Link of image, video or audio file
 */
export function validateMediaLink(mediaLink) {
  const extension = getMediaExtension(mediaLink);
  return config.mediaExtensions.indexOf(extension) != -1;
}

/**
 * Check the given link to match the valid file types
 * For example, mp3/audio -> true ogg/audio -> false
 *
 * @param {*} mediaLink Link of image, video or audio file
 * @param {*} type File type get from file input
 */
export function validateMediaLinkFromType(mediaLink, type) {
  const validTypes = config.mediaFilters[type];
  if (!Array.isArray(validTypes)) {
    return false;
  }

  const extension = getMediaExtension(mediaLink);
  return validTypes.indexOf(extension) != -1;
}

/**
 * Shorten a given string to a certain length
 *
 * @param {*} string String need to be cut
 * @param {*} length Length of the cut string from beginning
 */
export function trimString(string, length = 30) {
  if (!isString(string)) {
    return "";
  }
  if (string.length > length) {
    return string.substring(0, length - 1) + "...";
  } else {
    return string;
  }
}

/**
 * Shorten a given string but keeping certain numbers of chars
 * from both start and end of the string
 *
 * @param {*} string String need to be cut
 * @param {*} start The position cut from 0 to
 * @param {*} end The position cut at end to
 */
export function trimBothEnd(string, start = 20, end = 10) {
  const len = string.length;
  if (len > start + end + 3) {
    return string.substring(0, start) + "..." + string.substring(len - 10, len);
  } else {
    return string;
  }
}
