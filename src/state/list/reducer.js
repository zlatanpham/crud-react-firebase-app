// Internal
import {
  LIST_FETCH_ITEMS_REQUEST,
  LIST_FETCH_ITEMS_REQUEST_SUCCESS,
  LIST_FETCH_ITEMS_REQUEST_FAILURE,
  LIST_FETCH_ITEM_PER_PAGE,
  LIST_DELETE_ITEM_FROM_LIST,
  LIST_REFRESH_STORE,
  LIST_ADD_ITEM,
  LIST_UPDATE_ITEM,
  LIST_CHANGE_ITEM_PER_PAGE
} from "../action-types";

import config from "../../config";

let itemPerPage = config.itemPerPage;
const storedItemPerPage = parseInt(window.localStorage.getItem("itemPerPage"));
if (config.itemPerPageOptions.indexOf(storedItemPerPage) != -1) {
  itemPerPage = storedItemPerPage;
}

export default function reducer(
  state = {
    itemPerPage,
    items: [],
    needReorganize: false,
    current: 1,
    max: 1,
    pageItems: [],
    fetching: true,
    fetchError: false
  },
  action
) {
  switch (action.type) {
    case LIST_FETCH_ITEMS_REQUEST: {
      return { ...state, fetching: true };
    }
    case LIST_FETCH_ITEMS_REQUEST_SUCCESS: {
      if (state.items.length == 0) {
        return { ...state, items: action.items };
      }
      return state;
    }
    case LIST_FETCH_ITEMS_REQUEST_FAILURE: {
      return { ...state, fetchError: true };
    }
    case LIST_FETCH_ITEM_PER_PAGE: {
      let n = parseInt(action.n);
      if (isNaN(n) || n < 1) {
        n = 1;
      }
      const len = state.items.length;
      const max = Math.ceil(len / state.itemPerPage);

      const current = max >= n ? n : max;
      const start = (current - 1) * state.itemPerPage;
      const fetching = false;
      const pageItems = state.items.slice(start, start + state.itemPerPage);

      return { ...state, current, max, pageItems, fetching };
    }
    case LIST_DELETE_ITEM_FROM_LIST: {
      const items = state.items.filter(item => {
        return item.id !== action.id;
      });
      return { ...state, items };
    }
    case LIST_REFRESH_STORE: {
      if (state.fetching) {
        return state;
      }
      const len = state.items.length;
      const max = Math.ceil(len / state.itemPerPage);
      const current = state.current <= max ? state.current : max;
      const start = (current - 1) * state.itemPerPage;
      const pageItems = state.items.slice(start, start + state.itemPerPage);

      return { ...state, pageItems, max, current };
    }
    case LIST_ADD_ITEM: {
      if (state.fetching) {
        return state;
      }

      const items = [...state.items];
      items.unshift(action.item);
      return { ...state, items };
    }
    case LIST_UPDATE_ITEM: {
      if (state.fetching) {
        return state;
      }

      const updatedItem = action.updatedItem;
      const id = updatedItem.id;

      const indexInList = state.items.findIndex(item => item.id === id);
      const indexInPage = state.pageItems.findIndex(item => item.id);

      if (indexInList != -1) {
        state.items[indexInList] = updatedItem;
      }

      if (indexInPage != -1) {
        state.pageItems[indexInPage] = updatedItem;
      }

      return state;
    }
    case LIST_CHANGE_ITEM_PER_PAGE: {
      window.localStorage.setItem("itemPerPage", action.itemPerPage);
      return { ...state, itemPerPage: action.itemPerPage };
    }
    default: {
      return state;
    }
  }
}
