//External
import * as moment from "moment";

// Internal
import database, {
  addItemToDatabase,
  updateItemInDatabase,
  uploadFileToStorage,
  removeItemFromDatabase
} from "../../firebase-database";

import {
  LIST_FETCH_ITEMS_REQUEST_SUCCESS,
  LIST_FETCH_ITEMS_REQUEST_FAILURE,
  LIST_DELETE_ITEM_FROM_LIST,
  LIST_FETCH_ITEM_PER_PAGE,
  LIST_REFRESH_STORE,
  LIST_ADD_ITEM,
  LIST_UPDATE_ITEM,
  LIST_CHANGE_ITEM_PER_PAGE
} from "../action-types";

const listRef = database.ref().child("items");

/**
 *
 * @param {int} n Page Number
 */
export function fetchItems(n) {
  return dispatch => {
    listRef
      .once("value")
      .then(snapshot => {
        let items = [];

        snapshot.forEach(childSnapshot => {
          const childKey = childSnapshot.key;
          const childData = childSnapshot.val();
          const item = { ...childData, id: childKey };

          items.unshift(item);
        });

        dispatch({ type: LIST_FETCH_ITEMS_REQUEST_SUCCESS, items });
        dispatch(fetchItemsPerPage(n));
      })
      .catch(error => {
        dispatch({ type: LIST_FETCH_ITEMS_REQUEST_FAILURE, error });
      });
  };
}

export function deleteItem(id) {
  return dispatch => {
    return removeItemFromDatabase(id)
      .then(response => {
        dispatch({ type: LIST_DELETE_ITEM_FROM_LIST, id });
        dispatch({ type: LIST_REFRESH_STORE });

        return response;
      })
      .catch(error => {
        throw error;
      });
  };
}

export function exportCSV() {
  return function(dispatch) {
    return new Promise((resolve, reject) => {
      reject("Error");
    });
  };
}

export function fetchItemsPerPage(n) {
  return { type: LIST_FETCH_ITEM_PER_PAGE, n };
}

export function addItem(item) {
  return dispatch => {
    const { file } = item;
    if (file) {
      return uploadFileToStorage(file)
        .then(media_link => {
          const media_type = file.type.split("/")[0];
          const date_created = moment().format("DD MMM, YYYY");
          const { title, description } = item;
          const pushItem = {
            title,
            description,
            date_created,
            media_link,
            media_type
          };

          return addItemToDatabase(pushItem);
        })
        .then(newItem => {
          dispatch(addItemToList(newItem));
          return newItem;
        })
        .catch(error => {
          throw error;
        });
    } else {
      return addItemToDatabase(item).then(pushItem => {
        dispatch(addItemToList(pushItem));
        return pushItem;
      });
    }
  };
}

export function updateItem(item) {
  return dispatch => {
    const {
      file,
      id,
      title,
      media_link,
      description,
      date_created,
      media_type
    } = item;

    if (media_link) {
      return updateItemInDatabase({
        id,
        title,
        description,
        media_link,
        date_created,
        media_type
      })
        .then(updatedItem => {
          dispatch(updateItemInList(updatedItem));
          return updatedItem;
        })
        .catch(error => {
          return error;
        });
    } else {
      return uploadFileToStorage(file)
        .then(new_media_link => {
          const new_media_type = file.type.split("/")[0];

          return updateItemInDatabase({
            id,
            title,
            description,
            date_created,
            media_link: new_media_link,
            media_type: new_media_type
          });
        })
        .catch(error => {
          return error;
        })
        .then(updatedItem => {
          dispatch(updateItemInList(updatedItem));
          return updatedItem;
        })
        .catch(error => {
          return error;
        });
    }
  };
}

/**
 * Add new item to firebase store
 *
 * @param {Object} item
 */
export function addItemToList(item) {
  return dispatch => {
    dispatch({ type: LIST_ADD_ITEM, item });
    dispatch({ type: LIST_REFRESH_STORE });
  };
}

export function addItemAfterUploadMedia(item) {
  return dispatch => {
    const date_created = moment().format("YYYY-MM-DD");
    const pushItem = { ...item, date_created };

    return listRef
      .push(pushItem)
      .then(response => {
        const id = response.key;
        const newItem = { ...pushItem, id };

        dispatch({ type: LIST_ADD_ITEM, newItem });
        dispatch({ type: LIST_REFRESH_STORE });

        // return id for redirecting
        return id;
      })
      .catch(error => {
        throw error;
      });
  };
}

export function updateItemInList(updatedItem) {
  return { type: LIST_UPDATE_ITEM, updatedItem };
}

export function changeItemPerPage(num) {
  return dispatch => {
    const itemPerPage = parseInt(num);
    dispatch({ type: LIST_CHANGE_ITEM_PER_PAGE, itemPerPage });
    dispatch({ type: LIST_REFRESH_STORE });
  };
}
