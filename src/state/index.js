// External
import { combineReducers, applyMiddleware, createStore } from "redux";
import { routerMiddleware } from "react-router-redux";
import thunk from "redux-thunk";
import createHistory from "history/createBrowserHistory";
import { routerReducer } from "react-router-redux";

// Internal
import list from "./list/reducer";

const combineReducer = combineReducers({
  list,
  routing: routerReducer
});

export const history = createHistory();

const store = createStore(
  combineReducer,
  applyMiddleware(thunk, routerMiddleware(history))
);

export default store;
