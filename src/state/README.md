# STATE

This directory contains all the behavor describing the global application state. Folders within the directory reflect sub-strees of the global state. Each folder like `list` contains `actions` and `reducers` files to modify the state of the sub tree.

## Action Types

`actions-types` contains all actions which will be exported to action and reducer sub-tree files. The actions are named to indicate their own purposes.
