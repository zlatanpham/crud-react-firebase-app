# SRC

The folder is the heart of the project. It contains the main app components and logic code.

## Files

The files are created for their own purposes. Kept in `/src` as root.

* `config` - configuration settings.
* `utils` - shared functions to perform common tasks.
* `firebase-database` - Firebase initialization and functions to perfrom CRUD actions.

## Structure

The folders are organized for their own purposes. Kept in `/src` as root.

* `/components`: folder holds the shared components of the application.
* `/scenes`: folder holds the containers (as pages) and their child components.
* `/state`: folder holds the structure of application global state and data flows.
* `/scss`: folder holds the main sass files and the files imported from components.
