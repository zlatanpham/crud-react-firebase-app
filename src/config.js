const CONFIG = {
  mediaTypes: [
    "audio/mp3",
    "image/png",
    "image/jpg",
    "video/mp4",
    "image/jpeg"
  ],
  mediaFilters: {
    image: ["png", "jpg", "jpeg"],
    audio: ["mp3"],
    video: ["mp4"]
  },
  mediaExtensions: ["mp3", "png", "jpg", "mp4", "jpeg"],
  itemPerPageOptions: [5, 10, 15, 20],
  itemPerPage: 5
};

export const FIREBASE_CONFIG = {
  apiKey: "AIzaSyAuALeaTYax7IrGWqLxzUCi7T3cBkD5q4Q",
  authDomain: "react-crud-app-580ab.firebaseapp.com",
  databaseURL: "https://react-crud-app-580ab.firebaseio.com",
  projectId: "react-crud-app-580ab",
  storageBucket: "react-crud-app-580ab.appspot.com",
  messagingSenderId: "812137817696"
};

export default CONFIG;
