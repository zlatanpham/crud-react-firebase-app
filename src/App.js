// External
import React, { Component } from "react";
import { Route, Switch } from "react-router";

// Interal
import Navbar from "./components/navbar";
import List from "./scenes/list";
import Item from "./scenes/item";
import Search from "./scenes/search";
import PageNotFound from "./scenes/404";

import "./scss/style.scss";

const routes = [
  {
    path: "/new",
    component: Item,
    exact: true
  },
  {
    path: "/nasa-search",
    component: Search,
    exact: true
  },
  {
    path: "/item/:id",
    component: Item,
    exact: false
  },
  {
    path: "*",
    component: PageNotFound
  }
];

export default class App extends Component {
  render() {
    return (
      <div className="app">
        <Navbar />
        <div className="app__container container">
          <Switch>
            <Route path="/" exact component={List} />
            <Route path="/page/:n" exact component={List} />
            {routes.map((route, index) => {
              return route.exact ? (
                <Route
                  key={index}
                  exact
                  path={route.path}
                  component={route.component}
                />
              ) : (
                <Route
                  key={index}
                  path={route.path}
                  component={route.component}
                />
              );
            })}
          </Switch>
        </div>
      </div>
    );
  }
}
