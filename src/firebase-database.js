// External
import * as firebase from "firebase";
import * as moment from "moment";

// Internal
import { FIREBASE_CONFIG } from "./config";

firebase.initializeApp(FIREBASE_CONFIG);

const database = firebase.database();
const listRef = database.ref().child("items");

export const storage = firebase.storage();
const storageRef = storage.ref();
let uploadFileTask = null;

export function uploadFileToStorage(file) {
  const name = +new Date() + "-" + file.name;
  const metadata = {
    contentType: file.type
  };
  uploadFileTask = storageRef.child(name).put(file, metadata);

  return uploadFileTask
    .then(snapshot => {
      uploadFileTask = null;
      return snapshot.downloadURL;
    })
    .catch(error => {
      uploadFileTask = null;
      throw error;
    });
}

export function cancelUploadTask() {
  if (uploadFileTask !== null) {
    uploadFileTask.cancel();
  }
}

export function fetchItemFromDatabase(id) {
  return database
    .ref("/items/" + id)
    .once("value")
    .then(snapshot => {
      const itemData = snapshot.val();
      return itemData;
    })
    .catch(error => {
      throw error;
    });
}

export function addItemToDatabase(item) {
  const date_created = item.date_created || moment().format("DD MMM, YYYY");
  const pushItem = { ...item, date_created };
  const task = listRef.push(pushItem);

  return task
    .then(snapshot => {
      const id = snapshot.key;
      const newItem = { ...pushItem, id };

      return newItem;
    })
    .catch(error => {
      throw error;
    });
}

export function removeItemFromDatabase(id) {
  return database
    .ref(`/items/${id}`)
    .remove()
    .then(response => {
      return response;
    })
    .catch(error => {
      throw error;
    });
}

export function updateItemInDatabase(item) {
  const { id, ...updateItem } = item;

  return database
    .ref(`/items/${id}`)
    .update(updateItem)
    .then(snapshot => {
      return item;
    })
    .catch(error => {
      throw error;
    });
}

export default database;
