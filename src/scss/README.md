# SASS

Main stylesheet of the app.

## Modules

Each module contains its own purpose. Detail listed below:

* `classes`: common classes used with `@extend`.
* `components`: import sass files from `/src/components/*`.
* `mixins`: common functions used with `@include`.
* `scenes`: import sass files from `/src/scenes/*`.
* `variables`: global variables like colors and spacing.
