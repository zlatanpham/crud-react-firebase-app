# LAZY IMAGE

Component for loading an image before displaying it on screen.

## Usage

At a minimum, you must provide a `src` for your LazyImage.

```jsx
import LazyImage from "components/lazy-image";

export default function MyComponent() {
  return <LazyImage src="http://domain.com/my-image.jpg" />;
}
```

## Props

The following props are available to customize the component:

* `src`: String indicating the link of the image.
