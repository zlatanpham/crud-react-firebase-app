// External
import React, { Component } from "react";
import classnames from "classnames";
import { noop } from "lodash";
import PropTypes from "prop-types";

export default class LazyLoadImage extends Component {
  static propTypes = {
    src: PropTypes.string.isRequired
  };

  constructor(props) {
    super(props);

    this.state = {
      loading: true,
      error: false
    };
  }

  componentDidMount() {
    this.createLoader();
  }

  componentWillUnmount() {
    this.destroyLoader();
  }

  createLoader() {
    this.img = new Image();
    this.img.src = this.props.src;
    this.img.onload = () => {
      this.setState({
        loading: false
      });
    };

    this.img.onerror = () => {
      this.setState({
        error: true
      });
    };
  }

  destroyLoader() {
    if (!this.img) {
      return;
    }

    this.img.onload = noop;
    this.img.onerror = noop;
    delete this.img;
  }

  render() {
    if (this.state.error) {
      return <div className="alert alert-danger">Cannot load image.</div>;
    }

    return (
      <div
        className={classnames("lazyImage", {
          "lazyImage--loading": this.state.loading
        })}
      >
        <div
          className="lazyImage__holder"
          style={{ backgroundImage: `url(${this.props.src})` }}
        />
        <span className="lazyImage__spin" />
      </div>
    );
  }
}
