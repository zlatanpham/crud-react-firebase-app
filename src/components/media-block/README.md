# MEDIA BLOCK

Component for displaying media block (video, image or audio) based on the given url and media type.

## Usage

At a minimum, you must provide `link` as url of the media and `type` as the type of the media.

```jsx
import MediaBlock from "components/media-block";

export default function MyComponent() {
  return <MediaBlock link="http://domain.com/video.mp4" type="video" />;
}
```

## Props

The following props are available to customize the component:

* `link`: String and Required indicating the media url.
* `type`: String and Required indicating the media type, accept `video`, `audio` and `image`.
