// External
import React from "react";
import PropTypes from "prop-types";

// Internal
import LazyLoadImage from "../lazy-image";

export default function MediaBlock(props) {
  let content = null;
  const link = props.link;
  const type = props.type;

  switch (type) {
    case "video":
      content = (
        <div className="embed-responsive embed-responsive-16by9">
          <video className="embed-responsive-item" controls>
            <source src={link} type="video/mp4" />
          </video>
        </div>
      );
      break;
    case "image":
      content = <LazyLoadImage src={link} />;
      break;
    case "audio":
      content = (
        <audio className="w-100" controls>
          <source src={link} type="audio/mp3" />
        </audio>
      );
      break;
  }

  return content;
}

MediaBlock.propTypes = {
  link: PropTypes.string.isRequired,
  type: PropTypes.oneOf(["video", "image", "audio"])
};
