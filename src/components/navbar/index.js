// External
import React from "react";
import { NavLink } from "react-router-dom";

export default function Navbar(props) {
  return (
    <nav className="navbar navbar-expand-lg navbar-dark bg-primary">
      <div className="container">
        <button
          className="navbar-toggler"
          type="button"
          data-toggle="collapse"
          data-target="#navbarNav"
          aria-controls="navbarNav"
          aria-expanded="false"
          aria-label="Toggle navigation"
        >
          <span className="navbar-toggler-icon" />
        </button>
        <div className="collapse navbar-collapse" id="navbarNav">
          <ul className="navbar-nav bd-navbar-nav">
            <li className="nav-item">
              <NavLink
                exact={true}
                className="nav-link"
                activeClassName="nav-link--active"
                to="/"
              >
                Home
              </NavLink>
            </li>
            <li className="nav-item">
              <NavLink
                exact={true}
                className="nav-link"
                activeClassName="nav-link--active"
                to="/new"
              >
                New Item
              </NavLink>
            </li>
            <li className="nav-item">
              <NavLink
                exact={true}
                className="nav-link"
                activeClassName="nav-link--active"
                to="/nasa-search"
              >
                NASA Search
              </NavLink>
            </li>
          </ul>
        </div>
      </div>
    </nav>
  );
}
