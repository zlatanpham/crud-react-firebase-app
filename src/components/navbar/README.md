# NAVBAR

Component for the header navigation to provide route links to application pages.

## Usage

This is a stateless component and no require props.

```jsx
import Navbar from "components/navbar";

export default function MyComponent() {
  return <Navbar />;
}
```
