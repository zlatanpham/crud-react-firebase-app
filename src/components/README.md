# COMPONENTS

This folder contains common components used across the application. Each component is located inside a sub-tree folder encapsulating its logic, presentation and style.
