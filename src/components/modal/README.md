# MODAL

Component for Modal popup to display info.

## Usage

This modal is built by `createPortal`. As requirement, you must set the tag `<div id="modal"></div>` inside your HTML body.

```jsx
import Modal from "components/modal";

export default function MyComponent() {
  return (
    <Modal
      title="My Modal Example"
      onClose={() => {
        this.setState({ closeModal: true });
      }}
      extraClass="modal-lg"
      buttons={[
        {
          extraBtnClass: "btn-primary btn-sm",
          onclick: this.closeModal,
          text: "Cancel Modal"
        }
      ]}
    >
      This is my info message. HTML doc is also allowed.
    </Modal>
  );
}
```

## Props

The following props are available to customize the component:

* `title`: String indicating the title of the modal
* `extraClass`: String adding extra classes for the modal
* `onClose`: Function this must have to trigger to close the modal
* `buttons`: Array of Objects objects to render buttons on the modal footer
  * `-extraBtnClass` : String addtional classes for the button
  * `-text`: String an inner text of the button
  * `-onClick`: Function a callback after button click
