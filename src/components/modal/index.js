// External
import React, { Component } from "react";
import { createPortal } from "react-dom";
import PropTypes from "prop-types";

const modalRoot = document.getElementById("modal");

export default class Modal extends Component {
  static propTypes = {
    title: PropTypes.string,
    extraClass: PropTypes.string,
    onClose: PropTypes.func.isRequired,
    buttons: PropTypes.arrayOf(PropTypes.object)
  };

  constructor(props) {
    super(props);

    this.el = document.createElement("div");
    this.el.id = "app-modal";
    this.close = this.close.bind(this);
    this.buttonCallback = this.buttonCallback.bind(this);
  }

  componentDidMount() {
    modalRoot.appendChild(this.el);
  }

  componentWillUnmount() {
    modalRoot.removeChild(this.el);
  }

  close() {
    if (typeof this.props.onClose === "function") {
      this.props.onClose();
    }
  }

  buttonCallback() {
    this.close();
  }

  render() {
    const title = this.props.title || "Modal Title";

    let footer = null;

    if (Array.isArray(this.props.buttons)) {
      footer = (
        <div className="modal-footer">
          {this.props.buttons.map((btn, i) => {
            const extraBtnClass = btn.extraClass || "";
            return (
              <button
                key={i}
                className={"btn " + extraBtnClass}
                onClick={btn.onClick}
              >
                {btn.text}
              </button>
            );
          })}
        </div>
      );
    }

    return createPortal(
      <div className="modal fade show">
        <div
          className={
            "modal-dialog modal-dialog-centered " + this.props.extraClass
          }
          role="document"
        >
          <div className="modal-content">
            <div className="modal-header">
              <h5 className="modal-title">{title}</h5>
              <button type="button" className="close" aria-label="close">
                <span aria-hidden="true" onClick={this.close}>
                  ×
                </span>
              </button>
            </div>
            <div className="modal-body">{this.props.children}</div>
            {footer}
          </div>
        </div>
      </div>,
      this.el
    );
  }
}
