# 404

Use to render 404 page.

## Usage

Inject the component insite a container or a Route.

```jsx
import PageNotFound from "scenes/404";

export default function MyComponent() {
  return (
    <div className="myApp">
      <PageNotFound />
    </div>
  );
}
```

or with React route.

```jsx
import PageNotFound from "scenes/404";
import { Route } from "react-router";

export default function MyComponent() {
  return <Route path="*" component={PageNotFound} />;
}
```
