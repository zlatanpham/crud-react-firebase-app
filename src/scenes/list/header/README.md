# HEADER

The header container of the list page, containing export CSV and add item button.

## Usage

The component should be injected inside the parent `List` component.

```jsx
import ListHeader from "scenes/list/header";

export default function MyComponent() {
  return (
    <div className="myList">
      <ListHeader />
      <ListTable />
      <ListFooter />
    </div>
  );
}
```

## Child Components

### Export Button

This component is used to export all items from database to a CSV file. Its props are derived from `list` store via the observer `@connect` of Redux.

##### Usage

```jsx
import ExportButton from "scenes/list/header/export-button";

export default function MyComponent() {
  return (
    <div className="listHeader">
      <ExportButton />
    </div>
  );
}
```

##### Props from Store

* `items`: the array of items, the button will read the data to export the CSV file.
* `fetching`: the state to detemine whether the list of items was fetched from Firebase or not, this is used to activate the button.
