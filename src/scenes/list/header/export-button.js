// External
import React, { Component } from "react";
import { connect } from "react-redux";
import * as moment from "moment";

@connect(store => {
  return {
    items: store.list.items,
    fetching: store.list.fetching
  };
})
export default class ExportButton extends Component {
  constructor(props) {
    super(props);

    this.downloadCSV = this.downloadCSV.bind(this);
  }

  downloadCSV() {
    const columns = [
      "data:text/csv;charset=utf-8",
      "title",
      "description",
      "date_created",
      "media_type",
      "media_link"
    ];
    let csvContentHolder = [];
    csvContentHolder.push(columns.join(","));

    this.props.items.forEach(item => {
      const { title, description, date_created, media_type, media_link } = item;
      const row = [title, description, date_created, media_type, media_link];
      csvContentHolder.push(row.join(","));
    });

    const csvContent = csvContentHolder.join("\r\n");
    const encodeUri = encodeURI(csvContent);

    const link = document.createElement("a");
    link.href = encodeUri;
    link.download = `csv_data_${moment().format("DDMMYYYY")}.csv`;
    link.style.display = "none";
    document.body.appendChild(link);
    link.click();
    document.body.removeChild(link);
  }

  render() {
    return (
      <button
        type="button"
        onClick={this.downloadCSV}
        className="btn btn-dark"
        disabled={this.props.fetching || this.props.items.length == 0}
      >
        <i className="fas fa-download mr-2" />
        Export CSV
      </button>
    );
  }
}
