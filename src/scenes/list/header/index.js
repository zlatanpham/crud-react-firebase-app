import React, { Component } from "react";
import ExportButton from "./export-button";
import { Link } from "react-router-dom";

export default class ListHeader extends Component {
  render() {
    return (
      <div className="list__header row justify-content-between">
        <div className="col-6 text-left">
          <ExportButton />
        </div>
        <div className="col-6 text-right">
          <div className="btn-group" role="group">
            <button
              id="btnGroupDrop1"
              type="button"
              className="btn btn-primary dropdown-toggle"
              data-toggle="dropdown"
              aria-haspopup="true"
              aria-expanded="false"
            >
              Add New
            </button>
            <div className="dropdown-menu" aria-labelledby="btnGroupDrop1">
              <Link to="/new" className="dropdown-item">
                Add Item
              </Link>
              <Link to="/nasa-search" className="dropdown-item">
                NSA Search
              </Link>
            </div>
          </div>
        </div>
      </div>
    );
  }
}
