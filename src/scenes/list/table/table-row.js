// External
import React, { Component, Fragment } from "react";
import classnames from "classnames";
import PropTypes from "prop-types";

//Internal
import MediaButton from "./media-button";
import { trimString } from "../../../utils";
import { Link } from "react-router-dom";
import Modal from "../../../components/modal";
import store from "../../../state";
import { deleteItem } from "../../../state/list/actions";

class TableRow extends Component {
  static propTypes = {
    item: PropTypes.shape({
      title: PropTypes.string.isRequired,
      media_link: PropTypes.string.isRequired,
      media_type: PropTypes.oneOf(["image", "audio", "video"]).isRequired,
      description: PropTypes.string,
      date_created: PropTypes.string
    })
  };

  constructor(props) {
    super(props);
    this.deleteItem = this.deleteItem.bind(this);

    this.state = {
      deleting: false,
      deleteModal: false,
      deleteError: false
    };
  }

  deleteItem() {
    const id = this.props.item.id;

    this.setState({
      deleting: true
    });
    store
      .dispatch(deleteItem(id))
      .then(Response => {})
      .catch(error => {
        this.setState({ deleting: false, deleteError: true });
      });
  }

  render() {
    const item = this.props.item;

    const deleteModal = this.state.deleteModal ? (
      <Modal
        title="Delete Item"
        onClose={() => this.setState({ deleteModal: false })}
        extraClass="modal-sm"
        buttons={[
          {
            text: "Yes",
            extraClass: "btn-primary",
            onClick: () => {
              this.deleteItem();
              this.setState({ deleteModal: false });
            }
          },
          {
            text: "No",
            extraClass: "btn-secondary",
            onClick: () => this.setState({ deleteModal: false })
          }
        ]}
      >
        Are you sure to delete the item?
      </Modal>
    ) : null;

    const deleteErrorNotification = this.state.deleteError ? (
      <Modal
        title="Try it later"
        onClose={() => this.setState({ deleteError: false })}
        extraClass="modal-sm"
        buttons={[
          {
            text: "Cancel",
            extraClass: "btn-secondary",
            onClick: () => {
              this.setState({ deleteError: false });
            }
          }
        ]}
      >
        Cannot delete the item. Please try it later.
      </Modal>
    ) : null;

    return (
      <Fragment>
        <tr
          className={classnames({
            "table-danger deactive": this.state.deleting
          })}
        >
          <td>
            <Link to={`/item/${item.id}`}>{trimString(item.title, 50)}</Link>
          </td>
          <td>{trimString(item.description, 60)}</td>
          <td>{item.date_created}</td>
          <td>
            <MediaButton link={item.media_link} type={item.media_type} />
          </td>
          <td>
            <a
              className="btn btn-sm btn-link mr-2"
              href={item.media_link}
              target="_blank"
              download
            >
              <i className="fas fa-download" />
            </a>

            <Link className="btn btn-sm btn-link mr-2" to={`/item/${item.id}`}>
              <i className="fas fa-edit" />
            </Link>
            <button
              className="btn btn-sm btn-link"
              onClick={() => this.setState({ deleteModal: true })}
            >
              <i className="fas fa-trash-alt" />
            </button>
          </td>
        </tr>
        {deleteModal}
        {deleteErrorNotification}
      </Fragment>
    );
  }
}

export default TableRow;
