// External
import React, { Component, Fragment } from "react";
import PropTypes from "prop-types";

// Internal
import Modal from "../../../components/modal";
import MediaBlock from "../../../components/media-block";

export default class MediaButton extends Component {
  static propTypes = {
    link: PropTypes.string.isRequired,
    type: PropTypes.oneOf(["audio", "video", "image"]).isRequired
  };

  constructor(props) {
    super(props);
    this.state = {
      openModal: false
    };

    this.openMedia = this.openMedia.bind(this);
    this.closeMedia = this.closeMedia.bind(this);
  }

  openMedia() {
    this.setState({ openModal: true });
  }

  closeMedia() {
    this.setState({ openModal: false });
  }

  render() {
    if (!!!this.props.link || !!!this.props.type) {
      return null;
    }

    const type = this.props.type.toLowerCase();
    const link = this.props.link;
    const mediaModal = this.state.openModal ? (
      <Modal title={"Media"} onClose={this.closeMedia}>
        <MediaBlock type={type} link={link} />
      </Modal>
    ) : null;

    return (
      <Fragment>
        {mediaModal}
        <button
          type="button"
          className="btn btn-dark btn-sm"
          onClick={this.openMedia}
        >
          {type}
        </button>
      </Fragment>
    );
  }
}
