// External
import React from "react";
import PropTypes from "prop-types";

export default function TableHead(props) {
  const items = props.items.map(item => (
    <th scope="col" key={item}>
      {item}
    </th>
  ));
  return (
    <thead>
      <tr>{items}</tr>
    </thead>
  );
}

TableHead.propTypes = {
  items: PropTypes.arrayOf(PropTypes.string).isRequired
};
