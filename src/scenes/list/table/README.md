# TABLE

This component is reponsible for displaying a list of items. This component is a child component of `List`

## Usage

The component should be injected inside the parent `List` component. Its props are derived from the `list` store via the observer `@connect` of Redux.

```jsx
import ListTable from "scenes/list/table";

export default function MyComponent() {
  return (
    <div className="myList">
      <ListHeader />
      <ListTable />
      <ListFooter />
    </div>
  );
}
```

## Props from store

* `items`: Array of Object contains the items to render for a particular page number

## Child Components

### Table Head

Render the header of item list table. This is a child component of `ListTable`.

##### Usage

```jsx
import TableHead from "scenes/list/table/table-head";

export default function MyComponent() {
  const headItems = ["ID", "Title", "Description", "Date"];
  return (
    <table className="listPage__table table">
      <TableHead items={headItems} />
      <tbody>{list}</tbody>
    </table>
  );
}
```

##### Props

* `items`: Array the list contains the heading titles

### Table Row

Render item info on a table row. This component contains some functions like delete, edit the item. This is a child component of `ListTable`.

##### Usage

```jsx
import TableRow from "scenes/list/table/table-row";

export default class extends React.Component {
  render() {
    return (
      <div className="table">
        (items.map(item => (<TableRow item={item} key={item.id} />))
      </div>
    );
  }
}
```

##### Props

* `item`: Object the object contains properties for rendering and performing functions like delete or edit
* `-title`: String the item title
* `-media_link`: String the url of media
* `-media_type`: String expect `video`, `audio` and `image`
* `-description`: String the item description
* `-date_created`: String the date item was created

### Media Button

Display the given media on a popup after clicking on the component. This is a child component of `ListTable`.

##### Usage

```jsx
import MediaButton from "scenes/list/table/table-head";

export default function MyComponent() {
  return <MediaButton link={"http://domain.com/my-image.png"} type="image" />;
}
```

##### Props

* `link`: String the url of the media.
* `type`: String expect only `image`, `video` and `audio`
