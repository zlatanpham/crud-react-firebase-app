// External
import React, { Component } from "react";
import { connect } from "react-redux";
import PropTypes from "prop-types";

// Internal
import TableHead from "./table-head";
import TableRow from "./table-row";

@connect(store => {
  return {
    items: store.list.pageItems
  };
})
export default class ListTable extends Component {
  static propTypes = {
    items: PropTypes.arrayOf(PropTypes.object)
  };

  static defaultProps = {
    items: []
  };

  render() {
    const headItems = [
      "Title",
      "Description",
      "Date created",
      "Type",
      "Actions"
    ];

    const list = this.props.items.map(item => (
      <TableRow item={item} key={item.id} />
    ));
    return (
      <table className="listPage__table table">
        <TableHead items={headItems} />
        <tbody>{list}</tbody>
      </table>
    );
  }
}
