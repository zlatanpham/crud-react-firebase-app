import React, { Component, Fragment } from "react";
import ListHeader from "./header";
import ListTable from "./table";
import ListFooter from "./footer";
import { connect } from "react-redux";
import { fetchItems, fetchItemsPerPage } from "../../state/list/actions";
import { isInteger } from "lodash";
import { Redirect } from "react-router-dom";

@connect((store, props) => {
  return {
    items: store.list.pageItems,
    fetching: store.list.fetching,
    max: store.list.max
  };
})
export default class List extends Component {
  componentWillMount() {
    if (this.props.items.length == 0) {
      let n = 1;

      if (this.props.match && this.props.match.params.n) {
        n = parseInt(this.props.match.params.n);
      }

      this.props.dispatch(fetchItems(n));
    }
  }

  componentWillReceiveProps(nextProps) {
    let n = nextProps.match.params.n;
    if (this.props.match.params.n !== n) {
      this.props.dispatch(fetchItemsPerPage(n));
    }
  }

  render() {
    if (this.props.fetching) {
      return (
        <Fragment>
          <ListHeader />
          <p>Loading...</p>
        </Fragment>
      );
    }

    if (this.props.match.params.n) {
      const n = parseInt(this.props.match.params.n);

      if (isNaN(n)) {
        return <Redirect to="/" />;
      } else if (n > this.props.max) {
        return <Redirect to={"/page/" + this.props.max} />;
      }
    }

    return (
      <div className="listPage">
        <ListHeader />
        {this.props.items.length > 0 ? (
          <Fragment>
            <ListTable />
            <ListFooter />
          </Fragment>
        ) : (
          <div>There is no item.</div>
        )}
      </div>
    );
  }
}
