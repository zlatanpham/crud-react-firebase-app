// External
import React, { Component } from "react";
import { connect } from "react-redux";
import classnames from "classnames";
import { Link } from "react-router-dom";

@connect(store => {
  return {
    max: store.list.max,
    current: store.list.current
  };
})
export default class ListPagination extends Component {
  render() {
    if (this.props.max == 1) {
      return null;
    }

    let items = [];
    const max = this.props.max;
    const current = this.props.current;
    const dots = pagination(current, max);

    dots.forEach((dot, index) => {
      if (dot == "...") {
        items.push(
          <li key={dot + index} className="page-item disabled">
            <a className="page-link">...</a>
          </li>
        );
      } else {
        items.push(
          <li
            key={dot}
            className={classnames("page-item", { disabled: dot == current })}
          >
            <Link to={"/page/" + dot} className="page-link">
              {dot}
            </Link>
          </li>
        );
      }
    });

    if (max > 4) {
      const disabledPrev = current == 1;
      const prevPage = current <= 1 ? 1 : current - 1;
      items.unshift(
        <li
          key={"previous"}
          className={classnames("page-item", { disabled: disabledPrev })}
        >
          <Link to={`/page/${prevPage}`} className="page-link">
            Prev
          </Link>
        </li>
      );

      const disabledNext = current == max;
      const nextPage = current >= max ? max : current + 1;
      items.push(
        <li
          key={"next"}
          className={classnames("page-item", { disabled: disabledNext })}
        >
          <Link to={`/page/${nextPage}`} className="page-link">
            Next
          </Link>
        </li>
      );
    }

    return (
      <nav>
        <ul className="pagination">{items}</ul>
      </nav>
    );
  }
}

/**
 * Return array of paginations from the current page and total number of pages
 *
 * @source https://gist.github.com/kottenator/9d936eb3e4e3c3e02598
 *
 * @param {*} c Current page
 * @param {*} m Max number of pages
 */
function pagination(c, m) {
  var current = c,
    last = m,
    delta = 1,
    left = current - delta,
    right = current + delta + 1,
    range = [],
    rangeWithDots = [],
    l;

  for (let i = 1; i <= last; i++) {
    if (i == 1 || i == last || (i >= left && i < right)) {
      range.push(i);
    }
  }

  for (let i of range) {
    if (l) {
      if (i - l === 2) {
        rangeWithDots.push(l + 1);
      } else if (i - l !== 1) {
        rangeWithDots.push("...");
      }
    }
    rangeWithDots.push(i);
    l = i;
  }

  return rangeWithDots;
}
