# FOOTER

Render a search page for requesting items via NASA api, users can save the requested items to database.

## Usage

The component should be injected inside the parent `List` component.

```jsx
import ListFooter from "scenes/list/footer";

export default function MyComponent() {
  return (
    <div className="myList">
      <ListHeader />
      <ListTable />
      <ListFooter />
    </div>
  );
}
```

## Child Components

### Pagination

Display the pagination for list page. This compoment is rendered based on `props` passed from the `list` store.

##### Usage

Simply eject the component into your container. Its props are derived from `list` store via the observer `@connect` of Redux.

```jsx
import Pagination from "scenes/list/footer/pagination";

export default function MyComponent() {
  return (
    <div className="listFooter">
      <Pagination />
    </div>
  );
}
```

##### Props from Store

* `max`: the maximum number of pages based on total number of items and the number of item per page.
* `current`: the current page number.

### Item Per Page Selector

The select input to choose to display a particular number of items per page. This compoment is used to fire a change to the `itemPerPage` state of `list` store.

##### Usage

```jsx
import ItemPerPageSelector from "scenes/list/footer/item-per-page-selector";

export default function MyComponent() {
  return (
    <div className="listFooter">
      <ItemPerPageSelector />
    </div>
  );
}
```

##### Props from Store

* `itemPerPage`: the number of item per page.
