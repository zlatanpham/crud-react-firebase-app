// External
import React, { Component } from "react";
import { connect } from "react-redux";

// Internal
import config from "../../../config";
import { changeItemPerPage } from "../../../state/list/actions";

@connect(store => {
  return {
    itemPerPage: store.list.itemPerPage
  };
})
export default class ItemPerPageSelector extends Component {
  constructor(props) {
    super(props);

    this.state = { itemPerPage: this.props.itemPerPage };
    this.handleChange = this.handleChange.bind(this);
  }

  handleChange(e) {
    const itemPerPage = e.target.value;
    this.setState({ itemPerPage });
    this.props.dispatch(changeItemPerPage(itemPerPage));
  }

  render() {
    const options = config.itemPerPageOptions;

    return (
      <label className="itemPerPageSelector">
        Items per page:{" "}
        <select
          className="form-control form-control-sm"
          value={this.state.itemPerPage}
          onChange={this.handleChange}
        >
          {options.map(option => (
            <option key={option} value={option}>
              {option}
            </option>
          ))}
        </select>
      </label>
    );
  }
}
