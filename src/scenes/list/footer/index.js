// External
import React from "react";

// Internal
import ItemPerPageSelector from "./item-per-page-selector";
import Pagination from "./pagination";

export default function ListFooter() {
  return (
    <div className="row">
      <div className="col-6">
        <Pagination />
      </div>
      <div className="col-6 text-right">
        <ItemPerPageSelector />
      </div>
    </div>
  );
}
