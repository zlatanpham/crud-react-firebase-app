# LIST

Use to render a list of items from database.

## Usage

Passing no param to `Route` for rendering number of items from the start.

```jsx
import List from "scenes/list";
import { Route } from "react-router";

export default function MyComponent() {
  return <Route path="/" component={List} />;
}
```

Or passing `:n` for rendering items at a position based on the param and the number of item per page.

```jsx
import List from "scenes/list";
import { Route } from "react-router";

export default function MyComponent() {
  return <Route path="/page/:n" component={List} />;
}
```

## Route Params

* `n` - the paging cursor number used to divide a particular range of items per page.

## Child Components

* `/header`: contains export button and add new item button.
* `/table`: contains a table displaying a number of item for row.
* `/footer`: contains page pagination and item per page selector.
