# ITEM

Use to render Item related pages including Add Item and Edit Item page

## Usage

Passing no param to `Route` for rendering add new item page

```jsx
import PageNotFound from "scenes/item";
import { Route } from "react-router";

export default function MyComponent() {
  return <Route path="/new" component={Item} />;
}
```

Or passing `:id` for rendering edit item page

```jsx
import PageNotFound from "scenes/item";
import { Route } from "react-router";

export default function MyComponent() {
  return <Route path="/item/:id" component={Item} />;
}
```

## Route Params

* `id` - the item ID for fetching its data to fulfill the component state
