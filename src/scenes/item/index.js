// External
import React, { Component } from "react";
import classnames from "classnames";
import { Redirect } from "react-router-dom";
import { connect } from "react-redux";

// Internal
import { updateItem, addItem } from "../../state/list/actions";
import { fetchItemFromDatabase } from "../../firebase-database";
import MediaBlock from "../../components/media-block";
import config from "../../config";

@connect(store => {
  return {
    items: store.list.items
  };
})
export default class Item extends Component {
  static prop;
  constructor(props) {
    super(props);

    this.isUpdatePage = !!props.match.params.id;

    this.state = {
      title: "",
      description: "",
      media_link: "",
      media_type: "",
      date_created: "",
      file: null,
      updateDone: false,
      fileName: "",
      errors: {},
      fetching: this.isUpdatePage ? true : false,
      media: "Choose file...",
      saving: false,
      globalError: "",
      redirectToUpdatePage: false,
      createdID: "",
      itemNotFound: false
    };

    this.submit = this.submit.bind(this);
    this.handleChange = this.handleChange.bind(this);
    this.handleFileInputChange = this.handleFileInputChange.bind(this);

    this.uploadNewFile = this.uploadNewFile.bind(this);
    this.cancelUploadNewFile = this.cancelUploadNewFile.bind(this);
  }

  componentWillMount() {
    const id = this.props.match.params.id;
    if (!id) {
      return;
    }

    const index = this.props.items.findIndex(item => id === item.id);
    if (index != -1) {
      const {
        title,
        description,
        date_created,
        media_type,
        media_link
      } = this.props.items[index];
      this.setState({
        title,
        date_created,
        description,
        media_type,
        media_link,
        fetching: false
      });
    } else {
      fetchItemFromDatabase(id)
        .then(itemData => {
          const {
            title,
            description,
            media_type,
            date_created,
            media_link
          } = itemData;
          this.setState({
            title,
            description,
            date_created,
            media_type,
            media_link,
            fetching: false
          });
        })
        .catch(error => {
          this.setState({
            fetching: false,
            itemNotFound: "Item not found!"
          });
        });
    }
  }

  handleChange(e) {
    if (!!this.state.errors[e.target.name]) {
      let errors = { ...this.state.errors };
      delete errors[e.target.name];

      this.setState({
        [e.target.name]: e.target.value,
        updateDone: false,
        errors
      });
    } else {
      this.setState({
        [e.target.name]: e.target.value,
        globalError: "",
        updateDone: false
      });
    }
  }

  submit(e) {
    e.preventDefault();
    let errors = {};

    this.setState({
      globalError: "",
      updateDone: false
    });

    if (this.state.title === "") errors.title = "This field required value.";

    if (!this.state.media_link) {
      if (this.state.fileName === "") {
        errors.file = "This field required value.";
      } else if (config.mediaTypes.indexOf(this.state.file.type) == -1) {
        errors.file =
          "Accept only: " +
          config.mediaExtensions.map(type => `*.${type}`).join(", ");
      }
    }

    const isValid = Object.keys(errors).length === 0;

    if (isValid) {
      this.setState({ saving: true, globalError: "" });

      if (this.isUpdatePage) {
        this.updateItem();
      } else {
        this.addItem();
      }
    } else {
      this.setState({ errors });
    }
  }

  updateItem() {
    const id = this.props.match.params.id;
    const {
      title,
      description,
      media_link,
      date_created,
      media_type,
      file
    } = this.state;

    this.props
      .dispatch(
        updateItem({
          id,
          title,
          description,
          media_link,
          date_created,
          media_type,
          file
        })
      )
      .then(updatedItem => {
        this.setState({
          media_link: updatedItem.media_link,
          media_type: updatedItem.media_type,
          saving: false,
          updateDone: true
        });
      })
      .catch(error => {
        this.setState({
          saving: false,
          globalError: "Cannot update the item. Try it later!"
        });
      });
  }

  addItem() {
    const { title, description, media_type, media_link, file } = this.state;

    this.props
      .dispatch(addItem({ title, description, media_type, media_link, file }))
      .then(newItem => {
        const createdID = newItem.id;

        this.setState({
          saving: false,
          redirectToUpdatePage: true,
          createdID
        });
      })
      .catch(error => {
        this.setState({
          saving: false,
          globalError: "Cannot add the item. Try it later!"
        });
      });
  }

  handleFileInputChange(e) {
    const file = e.target.files[0];
    const fileName = file.name;
    const type = file.type;

    this.setState(prevState => {
      const errors = { ...prevState.errors };
      const isValid = config.mediaTypes.indexOf(type) != -1;

      if (!isValid) {
        errors.file =
          "Accept only: " +
          config.mediaExtensions.map(type => `*.${type}`).join(", ");
      } else {
        delete errors.file;
      }
      return {
        file,
        fileName,
        errors,
        globalError: "",
        updateDone: false
      };
    });
  }

  uploadNewFile(e) {
    e.preventDefault();
    this.tempStoreMediaLink = this.state.media_link;
    this.setState({
      media_link: "",
      globalError: "",
      updateDone: false
    });
  }

  cancelUploadNewFile(e) {
    e.preventDefault();
    this.setState({
      media_link: this.tempStoreMediaLink,
      globalError: "",
      updateDone: false
    });
  }

  render() {
    if (this.state.redirectToUpdatePage) {
      return <Redirect to={`/item/${this.state.createdID}`} />;
    }

    if (this.state.itemNotFound) {
      return (
        <div className="itemPage__notify alert alert-danger">
          Sorry, we cannot find the item you are looking for :((
        </div>
      );
    }

    if (this.state.fetching) {
      return <div>Loading...</div>;
    }

    const notification = !!this.state.globalError ? (
      <div className="itemPage__notify alert alert-danger">
        {this.state.globalError}
      </div>
    ) : this.state.updateDone ? (
      <div className="itemPage__notify alert alert-success">
        <i className="fas fa-check mr-2" /> Your item has been updated
        successfully.
      </div>
    ) : null;

    return (
      <div className="itemPage row">
        <div className="col-lg-8">
          <h2>{this.isUpdatePage ? "Edit Item" : "New Item"}</h2>
          <p>
            {this.isUpdatePage
              ? "Edit and update item on database."
              : "Add a new item to your list."}
          </p>
          <form
            className={classnames("itemPage__form", {
              "itemPage__form--deactive":
                !this.isUpdatePage && this.state.saving
            })}
            onSubmit={this.submit}
          >
            {notification}
            <div className="form-group row">
              <label className="col-sm-2 col-form-label" htmlFor="inputTitle">
                Title*:
              </label>
              <div className="col-sm-10">
                <input
                  type="text"
                  name="title"
                  className={classnames("form-control", {
                    "is-invalid": !!this.state.errors.title
                  })}
                  id="inputTitle"
                  value={this.state.title}
                  onChange={this.handleChange}
                />
                <div className="invalid-feedback">
                  {this.state.errors.title}
                </div>
              </div>
            </div>

            <div className="form-group row">
              <label className="col-sm-2 col-form-label" htmlFor="inputDesc">
                Description:
              </label>
              <div className="col-sm-10">
                <textarea
                  name="description"
                  className="form-control"
                  id="inputDesc"
                  onChange={this.handleChange}
                  value={this.state.description}
                />
              </div>
            </div>

            {this.state.media_link ? (
              <div className="form-group row">
                <label className="col-sm-2 col-form-label">Media File*:</label>
                <div className="col-sm-10">
                  <MediaBlock
                    link={this.state.media_link}
                    type={this.state.media_type}
                  />

                  <div className="itemPage__media text-right">
                    <a onClick={this.uploadNewFile} href="#">
                      Upload new file
                    </a>{" "}
                  </div>
                </div>
              </div>
            ) : (
              <div className="form-group row">
                <label className="col-sm-2 col-form-label" htmlFor="media-file">
                  Media File*:
                </label>
                <div className="col-sm-10">
                  <div className="custom-file">
                    <input
                      type="file"
                      className={classnames("custom-file-input", {
                        "is-invalid": !!this.state.errors.file
                      })}
                      onChange={this.handleFileInputChange}
                      name="file"
                      id="media-file"
                      ref={input => {
                        this.fileInput = input;
                      }}
                    />
                    <label className="custom-file-label" htmlFor="inputMedia">
                      {this.state.fileName}
                    </label>
                    <div className="invalid-feedback">
                      {this.state.errors.file}
                    </div>
                  </div>
                  {this.isUpdatePage ? (
                    <div className="itemPage__media text-right">
                      <a href="#" onClick={this.cancelUploadNewFile}>
                        Cancel
                      </a>
                    </div>
                  ) : null}
                </div>
              </div>
            )}

            <div className="form-group">
              <button
                type="submit"
                className="btn btn-primary my-1"
                disabled={this.state.saving}
              >
                {this.isUpdatePage ? "Update item" : "Submit item"}
              </button>
              <span
                className={classnames("itemPage__spinder my-1 mx-sm-3", {
                  fade: !this.state.saving
                })}
              />
            </div>
          </form>
        </div>
      </div>
    );
  }
}
