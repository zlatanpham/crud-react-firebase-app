// External
import React, { Component } from "react";
import { Link } from "react-router-dom";
import * as moment from "moment";
import Prototypes from "prop-types";

// Interal
import TabContentMedia from "./tab-content-media";
import { addItem } from "../../state/list/actions";
import store from "../../state";

export default class TabContent extends Component {
  static propTypes = {
    item: Prototypes.shape({
      href: Prototypes.string.isRequired,
      data: Prototypes.array.isRequired
    }).isRequired
  };

  constructor(props) {
    super(props);
    this.state = {
      media_link: null,
      status: "none",
      error: false
    };

    this.addToList = this.addToList.bind(this);
    this.setMediaLink = this.setMediaLink.bind(this);
  }

  componentWillReceiveProps(nextProps) {
    if (this.props.item.href !== nextProps.item.href) {
      this.setState({
        media_link: null,
        status: "none"
      });
    }
  }

  addToList() {
    if (!this.state.media_link) {
      return;
    }

    this.setState({
      status: "saving",
      error: false
    });

    const itemData = this.props.item.data[0];
    const { title, description, media_type } = itemData;
    const date_created = moment(itemData.date_created).format("DD MMM, YYYY");
    const media_link = this.state.media_link;

    store
      .dispatch(
        addItem({ title, description, date_created, media_type, media_link })
      )
      .then(createdItem => {
        this.setState({
          createdID: createdItem.id,
          status: "saved"
        });
      })
      .catch(error => {
        this.setState({
          status: "ready",
          error: true
        });
      });
  }

  setMediaLink(media_link) {
    this.setState({
      media_link,
      status: "ready"
    });
  }

  render() {
    const item = this.props.item;
    const itemData = item.data[0];
    let button = null;

    switch (this.state.status) {
      case "ready":
        button = (
          <button className="btn btn-primary" onClick={this.addToList}>
            Add to List
          </button>
        );
        break;
      case "saving":
        button = (
          <button className="btn btn-primary" disabled>
            Saving...
          </button>
        );
        break;
      case "saved":
        button = (
          <Link
            className="btn btn-primary"
            to={`/item/${this.state.createdID}`}
          >
            Edit the item
          </Link>
        );
        break;
      default:
        button = (
          <button className="btn btn-primary" disabled>
            Add to List
          </button>
        );
        break;
    }

    return (
      <div className="tab-content">
        <div className="row">
          <div className="col-9">
            <h3>{itemData.title}</h3>
          </div>
          <div className="col-3 text-right">{button}</div>
        </div>
        <dl className="row">
          <dt className="col-sm-3">Date Created: </dt>
          <dd className="col-sm-9">
            {moment(itemData.date_created).format("DD MMM, YYYY")}
          </dd>

          <dt className="col-sm-3">Media Type:</dt>
          <dd className="col-sm-9">{itemData.media_type}</dd>
        </dl>

        <TabContentMedia
          type={itemData.media_type}
          link={item.href}
          onLoadMediaLink={this.setMediaLink}
        />
        <p>{itemData.description}</p>
      </div>
    );
  }
}
