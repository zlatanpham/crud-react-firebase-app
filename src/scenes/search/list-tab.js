// External
import React from "react";
import PropTypes from "prop-types";

// Internal
import ListTabItem from "./list-tab-item";

export default function ListTab(props) {
  return (
    <div className="list-group">
      {props.items.map(item => (
        <ListTabItem
          key={item.href}
          item={item}
          onSetActiveItem={props.onSetActiveItem}
        />
      ))}
    </div>
  );
}

ListTab.propTypes = {
  items: PropTypes.arrayOf(PropTypes.object)
};
