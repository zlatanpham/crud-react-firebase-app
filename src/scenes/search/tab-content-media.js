// External
import React, { Component } from "react";
import PropTypes from "prop-types";
import axios from "axios";

// Internal
import MediaBlock from "../../components/media-block";
import { getMediaExtension, validateMediaLinkFromType } from "../../utils";

const CancelToken = axios.CancelToken;
let cancel;

export default class TabContentMedia extends Component {
  static propTypes = {
    link: PropTypes.string.isRequired,
    type: PropTypes.oneOf(["audio", "video", "image"]),
    onLoadMediaLink: PropTypes.func.isRequired
  };

  constructor(props) {
    super(props);

    this.state = {
      loading: true,
      error: null,
      media_link: ""
    };
    this.fetchLink = this.fetchLink.bind(this);
  }

  componentWillReceiveProps(nextProps) {
    if (this.props.link !== nextProps.link) {
      this.fetchLink(nextProps.link);
    }
  }

  componentWillMount() {
    this.fetchLink(this.props.link);
  }

  fetchLink(link) {
    this.setState({
      loading: true,
      error: null
    });

    if (typeof cancel === "function") {
      cancel();
    }

    axios
      .get(link, {
        cancelToken: new CancelToken(function executor(c) {
          cancel = c;
        })
      })
      .then(res => {
        let links = res.data;
        let media_link = "";
        let isValid = false;
        const type = this.props.type.toLowerCase();

        for (let i = 0, len = links.length; i < len; i++) {
          const link = links[i];
          isValid = validateMediaLinkFromType(link, type);
          if (isValid) {
            media_link = link;
            break;
          }
        }

        if (isValid) {
          this.setState({
            media_link,
            loading: false
          });

          this.props.onLoadMediaLink(media_link);
        } else {
          const mediaTail = getMediaExtension(links[0]);
          this.setState({
            loading: false,
            error: `We do not support .${mediaTail} media type.`
          });
        }
      })
      .catch(err => {
        this.setState({
          error: true
        });
      });
  }

  render() {
    if (!!this.state.error) {
      return <div className="alert alert-danger">{this.state.error}</div>;
    }

    if (this.state.loading) {
      return <div className="alert alert-secondary">Loading media....</div>;
    } else {
      return (
        <div>
          <MediaBlock type={this.props.type} link={this.state.media_link} />
          <br />
        </div>
      );
    }
  }
}
