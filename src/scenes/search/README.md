# SEARCH

Render a search page for requesting items via NASA api, users can save the requested items to database.

## Usage

Inject the component insite a container or a Route.

```jsx
import Search from "scenes/search";

export default function MyComponent() {
  return (
    <div className="myApp">
      <Search />
    </div>
  );
}
```

or

```jsx
import Search from "scenes/search";
import { Route } from "react-router";

export default function MyComponent() {
  return <Route path="/search" component={Search} />;
}
```

## Child Components

### ListTab

A stateless component used to rendering a list of items getting from `items` props passing from the parent component.

##### Usage

```jsx
import ListTab from "scenes/search/list-tab";

export default function MyComponent() {
  constructor(props){
    this.states = {
      items,
      activeItem
    }
  }

  setActiveItem(item){
    this.setState({activeItem: item});
  }

  return (
    <ListTab items={this.state.items} onSetActiveItem={this.setActiveItem} />
  );
}
```

##### Props

* `items`: Array a list of items requested from the NASA api.
* `onSetActiveItem`: Function passing a callback function to change the active item state of its parent component.

### ListTabItem

The child component of `ListTab`, used to render a block with item title.

##### Usage

```jsx
import ListTabItem from "scenes/search/list-tab-item";

export default function MyComponent() {
  return <ListTabItem item={item} onSetActiveItem={setActiveItem} />;
}
```

##### Props

* `item`: Object used to passing the title and determine the active state.
  * `-title`: String the item title.
  * `-active`: Boolean the value to evaluate wheter the item should be highlighted.
* `onSetActiveItem`: Function and Required passing a callback function to change the active item state of its parent component.

### TabContent

Render info including title, description, media of an given NASA item. This component includes function to add the item to the list.

##### Usage

```jsx
import TabContent from "scenes/search/tab-content";

export default function MyComponent() {
  setActiveItem(item){
    this.setState({activeItem: item})
  }
  return <TabContent item={activeItem} />;
}
```

##### Props

* `item`: Object
  * `-href`: String the link to download an array containing media urls.
  * `-data`: Array the holder of item information.

### TabContentMedia

The child component of `TabContent`, used to load the media urls from `href` passing from `TabContent` and return the link if it is valid.

##### Usage

```jsx
import TabContentMedia from "scenes/search/tab-content-media";

export default function MyComponent() {
  setMediaLink(link){
    this.setState({media_link: link})
  }
  return (
    <TabContentMedia link={Link} type={type} onLoadMediaLink={setMediaLink} />
  );
}
```

##### Props

* `link`: String the url to get list of media urls.
* `type`: String accept one of value `video`, `media` or `image` as media type.
* `onLoadMediaLink`: Function a callback to set the `media_link` state of parent component after validating and loading the urls from `link`.
