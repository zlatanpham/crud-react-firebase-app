// External
import React, { Component } from "react";
import axios from "axios";

// Internal
import ListTab from "./list-tab";
import TabContent from "./tab-content";

const CancelToken = axios.CancelToken;
let cancel;

export default class Search extends Component {
  constructor(props) {
    super(props);
    this.handleChange = this.handleChange.bind(this);
    this.setActiveItem = this.setActiveItem.bind(this);

    this.state = {
      query: "",
      loading: false,
      typingTimeOut: 0,
      items: [],
      activeItem: null,
      currentIndex: -2
    };
  }

  handleChange(e) {
    if (this.state.typingTimeOut) {
      clearTimeout(this.state.typingTimeOut);
    }
    const value = e.target.value;

    if (value.length < 3) {
      this.setState({
        query: value,
        loading: false,
        currentIndex: -2,
        activeItem: null
      });

      return;
    }

    this.setState({
      query: value,
      loading: true,
      activeItem: null,
      currentIndex: -2,
      typingTimeOut: setTimeout(() => {
        if (typeof cancel === "function") {
          cancel();
        }

        axios
          .get(`https://images-api.nasa.gov/search`, {
            params: {
              q: this.state.query
            },
            cancelToken: new CancelToken(function executor(c) {
              cancel = c;
            })
          })
          .then(response => {
            const items = response.data.collection.items;
            let activeItem = null;
            let currentIndex = -1;
            if (items.length > 0) {
              currentIndex = 0;
              activeItem = items[currentIndex];
              items[currentIndex].active = true;
            }
            this.setState({
              currentIndex,
              items,
              activeItem,
              loading: false
            });
          })
          .catch(error => {
            this.setState({
              loading: false
            });
          });
      }, 500)
    });
  }

  setActiveItem(item) {
    this.setState(prevState => {
      const items = prevState.items;
      const index = items.findIndex(itm => itm.href === item.href);

      if (index != -1 && index != prevState.currentIndex) {
        const activeItem = prevState.items[index];
        items[prevState.currentIndex].active = false;
        activeItem.active = true;

        return {
          items,
          activeItem,
          currentIndex: index
        };
      }

      return prevState;
    });
  }

  render() {
    let searchInfo = null;
    if (this.state.loading) {
      searchInfo = <div>Searching...</div>;
    } else if (this.state.currentIndex == -1) {
      searchInfo = <div>Found no result!</div>;
    }

    return (
      <div className="search">
        <div className="search__header row">
          <div className="col-sm-6">
            <h2>Search from NASA</h2>
            <p>
              <input
                type="text"
                name="searchItem"
                value={this.state.query}
                onChange={this.handleChange}
                placeholder="For example: earth, moon..."
                className="form-control"
              />
            </p>
            {searchInfo}
          </div>
          <div className="col-sm-3" />
          <div className="col-sm-3" />
        </div>

        {this.state.currentIndex > -1 ? (
          <div className="row">
            <div className="col-4">
              <ListTab
                items={this.state.items}
                onSetActiveItem={this.setActiveItem}
              />
            </div>

            <div className="col-8">
              <TabContent item={this.state.activeItem} />
            </div>
          </div>
        ) : null}
      </div>
    );
  }
}
