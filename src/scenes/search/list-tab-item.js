// External
import React from "react";
import classnames from "classnames";
import PropTypes from "prop-types";

// Internal
import { trimBothEnd } from "../../utils";

export default function ListTabItem(props) {
  return (
    <a
      className={classnames("list-group-item list-group-item-action", {
        active: props.item.active
      })}
      onClick={() => props.onSetActiveItem(props.item)}
    >
      {trimBothEnd(props.item.data[0].title, 20, 10)}
    </a>
  );
}

ListTabItem.propTypes = {
  item: PropTypes.shape({
    title: PropTypes.string,
    active: PropTypes.bool
  }),
  onSetActiveItem: PropTypes.func.isRequired
};
