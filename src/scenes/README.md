# SCENES

The application is divided into scenes. Each scene reflects a sub folder, inside contains the components tailored for that scene.

## Scenes

* `404`: not found page.
* `item`: add new and edit item page.
* `list`: list of items page.
* `search`: search page using NASA api.
